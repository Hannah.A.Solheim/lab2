package INF101.lab2.pokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static Pokemon pokemon1 = new Pokemon("Pikachu", 94, 12);
    public static Pokemon pokemon2 = new Pokemon("Oddish", 100, 3);
    public static void main(String[] args) {
        ///// Oppgave 6
        // Have two pokemon fight until one is defeated
        pokemon1.toString();
        pokemon2.toString();

        while (pokemon1.getCurrentHP() > 0 && pokemon2.getCurrentHP() > 0){
            pokemon1.attack(pokemon2);
            if (pokemon2.getCurrentHP() == 0){
                break;
            }
            pokemon2.attack(pokemon1);
        }
    }
}
